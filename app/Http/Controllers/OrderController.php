<?php

namespace App\Http\Controllers;

use App\Api\Invoice;
use App\Api\Order;
use App\Api\Service;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function getAllOrder(Request $request){
        $order = null;
        if($request->role_id == 2){
            $order = Order::where('pelanggan_id' , $request->id)->with(['bengkel','service' , 'invoice'])->orderBy('id' , 'asc')->get();
        }elseif($request->role_id == 3){
            $order = Order::where('bengkel_id' , $request->id)->with(['pelanggan','service' , 'invoice'])->orderBy('id' , 'asc')->get();
        }
        return response($order , 200);
    }
    
    public function getPaginateOrder(Request $request){
        $order = Order::where('user_id' , $request->user_id)->with(['pelanggan','service'])->orderBy('id' , 'asc')->limit(10)->get();
        return response($order , 200);
    }

    public function getDetailOrder(Request $request){
        $order = null;
        if($request->role_id == 2){
            $order = Order::where('id' , $request->id)->with(['bengkel','service' , 'invoice'])->first();
        }elseif($request->role_id == 3){
            $order = Order::where('id' , $request->id)->with(['pelanggan','service' , 'invoice'])->first();
        }
        return response($order , 200);
    }

    public function filterStatus(Request $request){
        
        if($request->role_id == 2){
            $order = Order::where('pelanggan_id' , $request->id)->where('status' , $request->status)->with(['bengkel','service'])->orderBy('id' , 'asc')->get();
        }elseif($request->role_id == 3){
            if($request->status == 'dikerjakan'){
            $order = Order::where('bengkel_id' , $request->id)->where('status' , $request->status)->with(['pelanggan','service'])->orderBy('id' , 'asc')->get();
            }else{
                    $order = Order::where('bengkel_id' , $request->id)->where('status' , $request->status)->with(['pelanggan','service'])->orderBy('id' , 'asc')->get();
                }
            }
            return response($order , 200);
    }

    public function filterKeyword(Request $request){
        if($request->role_id == 2){
            $order = Order::leftJoin('services' , 'orders.service_id' , 'services.id')->where('orders.pelanggan_id' , $request->id)->where('services.nama_layanan' , 'like', '%' . $request->keyword . '%')->with(['bengkel','service'])->orderBy('id' , 'asc')->get();
        }elseif($request->role_id == 3){
            $order = Order::leftJoin('services' , 'orders.service_id' , 'services.id')->where('orders.bengkel_id' , $request->id)->where('services.nama_layanan' , 'like', '%' . $request->keyword . '%')->orderBy('id' , 'asc')->with(['pelanggan','service'])->get();
        }
        if($order){
            return response($order , 200);
        }else{
            return response('Tidak ditemukan' , 500);
        }
    }

    public function updateStatus(Request $request){
        if($request->status == 'dikerjakan' && Order::where('bengkel_id', $request->user_id)->where('status' , 'dikerjakan')->get()->count() == 4){
            return response('Pesanan yang sedang dikerjakan sudah mencapai batas maksimum' , 500);
        } else {
            $order = Order::where('id' , $request->id)->first();
            $order->status = $request->status;
            if($request->status == 'dikerjakan'){
                $order->start_time = date('H:i:s' , time());
            }
            if($request->status == 'selesai'){
                $order->finish_time = date('H:i:s' , time());
            }
            if($request->keterangan){
                $order->keterangan = $request->keterangan;
            }
            $order->save();
            return response($order , 200);
        }
    }
    
    public function store(Request $request){
        $service = Service::where('nama_layanan' , $request->service)->first();
        $user = User::where('nama' , $request->pelanggan)->first();
        $bengkel = User::where('id', $request->bengkel_id)->first();
        $order = new Order;
        if($user){
            $order->pelanggan_id = $user->id;
        }else{
            $user = new User();
            $user->nama = $request->pelanggan;
            $user->password = bcrypt('password');
            $user->email = 'pelanggan_' . $user->id . '@gmail.com';
            $user->kota = $bengkel->kota;
            $user->alamat = $bengkel->alamat;
            $user->nohp = $bengkel->nohp;
            $user->role_id = 2;
            $user->save();
            $user->email = 'pelanggan_' . $user->id . '@gmail.com';
            $user->save();
            $order->pelanggan_id = $user->id;
        }
        $order->status = $request->status;
        $order->waktu_dimulai = $request->waktu_dimulai;
        $order->waktu_selesai = $request->waktu_selesai;
        $order->bengkel_id = $request->bengkel_id;
        $order->service_id = $service->id;
        $order->save();

        $invoice = new Invoice();
        $invoice->order_id = $order->id;

        $order_id = $order->id .'/' . $bengkel->id . '/'. $service->id . '/' . Str::upper(Str::random(5));
        $date_no = date('dmY', time());
        $invoice->no_invoice = $order_id . '/' . $date_no;
        $invoice->service_id = $service->id;
        $invoice->user_id = $bengkel->id;
        $invoice->save();
        return response($order , 200);
    }
    public function invoice(){
        $order_id = '1/' . Str::upper(Str::random(5));
        $date_no = date('dmY', time());
        $invoice = $order_id . '/' . $date_no;
        return $invoice;
    }

}
