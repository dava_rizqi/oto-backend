<?php

namespace App\Http\Controllers;

use App\Analytic;
use App\Contact;
use App\Post;
use App\Service;
use App\Website;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $title = json_decode(Website::where('key' , 'title')->first()->value , false);
        $data['title'] = 'IDEAKITA.ID - ' . Str::upper($title);
        $visit = Analytic::where('field' , 'website_visit')->first();
        visits($visit)->increment();
        
        $data['filter'] = null;
        $data['post'] = Post::where('category' , 'our ideas')->where('is_active' , 1)->latest('created_at')->paginate(8);
        return view('front.home', $data);
    }

    public function about()
    {
        $data['title'] = 'About Us';
        $data['background'] = json_decode(Website::where('key' , 'background-image')->where('group' , 'about')->first()->value , false);
        $p1 = Website::where('key' , 'paragraph-1')->where('group' , 'about')->first();
        if($p1){
            $data['paragraph_1'] = json_decode($p1->value , false);
        }else{
            $data['paragraph_1'] = null;
        }
        $p2 = Website::where('key' , 'paragraph-2')->where('group' , 'about')->first();
        if($p2){
            $data['paragraph_2'] = json_decode($p2->value , false);
        }else{
            $data['paragraph_2'] = null;
        }
        $p3 = Website::where('key' , 'paragraph-3')->where('group' , 'about')->first();
        if($p3){
            $data['paragraph_3'] = json_decode($p3->value , false);
        }else{
            $data['paragraph_3'] = null;
        }
        
        $photo_1 = Website::where('key' , 'photo-1')->where('group' , 'about')->first();
        if($photo_1){
            $data['photo_1'] = json_decode($photo_1->value , false);
        }else{
            $data['photo_1'] = null;
        }
        $photo_2 = Website::where('key' , 'photo-2')->where('group' , 'about')->first();
        if($photo_2){
            $data['photo_2'] = json_decode($photo_2->value , false);
        }else{
            $data['photo_2'] = null;
        }
        $photo_3 = Website::where('key' , 'photo-3')->where('group' , 'about')->first();
        if($photo_3){
            $data['photo_3'] = json_decode($photo_3->value , false);
        }else{
            $data['photo_3'] = null;
        }
        return view('front.about', $data);
    }

    public function filter(Request $request)
    {
        $data['title'] = 'IDEAKITA.ID - Explore, Share, and Publish';
        if($request->filter == 'wwd'){
            $data['filter'] = 1;
            $data['post'] = Post::where('category' , 'our ideas')->where('is_active' , 1)->where('id_user' , $data['filter'])->latest('created_at')->paginate(50);
        }elseif($request->filter == 'wwl'){
            $data['filter'] = 2;
            $data['post'] = Post::where('category' , 'our ideas')->where('is_active' , 1)->where('id_user' , $data['filter'])->latest('created_at')->paginate(50);
        }else{
            $data['filter'] = $request->keyword;
            $data['post'] = Post::where('title' ,'like', '%' . $data['filter'] . '%')->where('is_active' , 1)->latest('created_at')->paginate(50);
        }
        return view('front.home', $data);
    }

    public function contest(){
        $data['title'] = 'IDEAKITA.ID - Explore, Share, and Publish | Contest List';
        $data['post'] = Post::with('visits')->where('category' , 'contest')->where('is_active' , 1)->latest('created_at')->paginate(8);
        return view('front.contest' , $data);
    }

    public function submitYourDesign(){
        $data['title'] = 'Publish your design | IDEAKITA.ID';
        $email = Website::where('key' , 'email')->first();
        $data['email'] = json_decode($email->value , false);
        $map = Website::where('key' , 'map')->first();
        $data['map'] = json_decode($map->value , false);
        return view('front.submit-design', $data);
    }
    public function service()
    {
        $data['title'] = 'Our Services';
        $data['service'] = Service::all();
        return view('front.service', $data);
    }

    public function contact()
    {
        $data['title'] = 'Contact Us | IDEAKITA.ID';
        $email = Website::where('key' , 'email')->first();
        $data['email'] = json_decode($email->value , false);
        $map = Website::where('key' , 'map')->first();
        $data['map'] = json_decode($map->value , false);
        $address = Website::where('key' , 'address')->first();
        $data['address'] = json_decode($address->value , false);
        $phone = Website::where('key' , 'phone')->first();
        $data['phone_number'] = json_decode($phone->value , false);
        return view('front.contact', $data);
    }

    public function contactMe(Request $request){
        $contact = new Contact;
        if(!$request->category || !$request->message ) {
            return redirect()->back()->withErrors('Pick the message categories');
        }
        $contact->message = $request->message;
        $contact->email = $request->email;
        $contact->category = $request->category;
        $contact->name = $request->name;
        $contact->phone_number = $request->phone_number;
        $contact->save();
        alert()->success('Wait for the reply in your email address', 'Your message has been sent!');
        return redirect()->back()->with('success', 'Your message has been sent'); 
    }

    public function post(){
        $data['post'] = Post::with('visits')->latest('created_at')->get();
        return view('front.post' , $data);
    }

    public function postDetail($id , $slug){
        $post = Post::find($id);
        $data['title'] = $post->title;
        $data['post'] = $post;
        $data['visit'] = $post->visits()->count();
        $data['posts'] = Post::limit(3)->latest('created_at')->get();
        $data['category'] = ['Softskills' , 'Assignment' , 'Tutorial' , 'Sharing'];
        // dd($data['posts']);
        // return view('front.post-detail' , $data);

        // dd($post);
        visits($post)->increment();
        return view('front.post-detail' , $data);
    }
}
