<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function login (Request $request ){
        $user = User::where('email' , $request->email)->first();
        if($user){
            if (Hash::check($request->password, $user->password))
            {
                return response($user , 200);
            }else {
                return response('password did not match' , 500);
            }
        }else {
            return response('Account not found' , 500);
        }
    }

    public function register (Request $request ){
        $user = User::where('email' , $request->email)->first();
        if($user){
            return 'Account already exist';
        }else {
            $user = new User();
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();
            return response($user , 200);
        }
    }
    public function completeRole(Request $request){
        $user = User::where('email' , $request->email)->first();
        if($user){
            $user->role_id = $request->role_id;
            $user->save();
            return response($user , 200);
        }else {
            return response('Account not found' , 500);
        }
    } 
    public function completeProfile(Request $request){
        $user = User::where('email' , $request->email)->first();
        if($user){
            $user->nama = $request->nama;
            $user->kota = $request->kota;
            $user->alamat = $request->alamat;
            $user->nohp = $request->nohp;
            $user->save();
            return response($user , 200);
        }else {
            return response('Account not found' , 500);
        }
    } 
    public function editProfile(Request $request){
        $user = User::where('email' , $request->old_email)->first();
        if($user){
            $user->nama = $request->nama;
            $user->kota = $request->kota;
            $user->alamat = $request->alamat;
            if($request->email){
                $user->email = $request->email;
            }
            if($request->password){
                $user->password = bcrypt($request->password);
            }
            if($request->role_id == 3){
                $user->jam_buka = $request->jam_buka;
                $user->jam_tutup = $request->jam_tutup;
            }
            $user->nohp = $request->nohp;
            $user->save();
            return response($user , 200);
        }else {
            return response('Account not found' , 500);
        }
    } 
    public function getAllUser(Request $request)
    {
        $user = null;
        if($request->role_id == 2){
            $user = User::where('role_id' , 3)->with(['order', 'service' , 'role'])->get();
        }elseif($request->role_id == 3){
            $user = User::where('role_id' , 2)->with(['order', 'service' , 'role'])->get();
        }
        return response($user , 200);
    } 
    public function searchUser(Request $request)
    {
        $user = null;
        if($request->role_id == 2){
            $user = User::where('role_id' , 3)->where('nama' , 'like' , '%' . $request->keyword . '%')->with(['order', 'service' , 'role'])->get();
        }elseif($request->role_id == 3){
            $user = User::where('role_id' , 2)->with(['order', 'service' , 'role'])->get();
        }
        return response($user , 200);
    }
    public function getPaginateUser(Request $request)
    {
        $user = null;
        if($request->role_id == 2){
            $user = User::where('role_id' , 3)->where('kota' , $request->kota)->with(['order', 'service' , 'role'])->get();
        }elseif($request->role_id == 3){
            $user = User::where('role_id' , 2)->with(['order', 'service' , 'role'])->get();
        }
        return response($user , 200);
    }
    public function getDetailUser(Request $request)
    {
        $user = null;
        if($request->role_id == 2){
            $user = User::where('id', $request->id)->with(['order', 'service' , 'role'])->first();
        }elseif($request->role_id == 3){
            $user = User::where('id', $request->id)->with(['order', 'service' , 'role'])->first();
        }
        return response($user , 200);
    }

}
