<?php

namespace App\Http\Controllers;

use App\Api\Service;
use App\Api\ServiceCategory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;

class ServiceController extends Controller
{
    public function getAllService (Request $request){
        $service = Service::where('user_id' , $request->user_id)->get();
        return response($service , 200);
    }
    public function getAllCategories (Request $request){
        $service = ServiceCategory::where('user_id' , $request->user_id)->get();
        return response($service , 200);
    }
    public function getDetailService(HttpFoundationRequest $request){
        $service = Service::where('nama_layanan' , $request->nama_layanan)->where('user_id' , $request->user_id)->with('category')->first();
        return $service;
    }
    public function store(Request $request){
        $service = new Service();
        $service->user_id = $request->user_id;
        $service->nama_layanan = $request->nama_layanan;
        $service->harga_layanan = $request->harga_layanan;
        $service->waktu_layanan = $request->waktu_layanan;
        $service->service_category_id = $request->service_category_id;
        $service->save();
        return response($service , 200);
    }
    public function storeCategory (Request $request) {
        $service = new ServiceCategory();
        $service->nama_kategori = $request->nama_kategori;
        $service->user_id = $request->user_id;
        $service->save();
        return response($service , 200);
    }
    public function update(Request $request){
        $service = Service::where('id' , $request->id)->first();
        $service->nama_layanan = $request->nama_layanan;
        $service->harga_layanan = $request->harga_layanan;
        $service->waktu_layanan = $request->waktu_layanan;
        $service->service_category_id = $request->service_category_id;
        $service->save();
        return response($service , 200);
    }
    public function deleteCategory(Request $request){
        $service = ServiceCategory::where('id' , $request->id)->first();
        $service->delete();
        return response($service , 200);
    }
    public function destroy(Request $request){
        $service = Service::where('id' , $request->id)->first();
        $service->delete();
        return response($service , 200);
    }
}
