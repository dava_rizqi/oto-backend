<?php

namespace App\Api;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function category(){
        return $this->belongsTo(ServiceCategory::class, 'service_category_id', 'id');
    }
}
