<?php

namespace App\Api;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    //
    public function services () {
        return $this->hasMany(Service::class);
    }
}
