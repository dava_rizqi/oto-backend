<?php

namespace App\Api;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Order extends Model
{
    public function pelanggan(){
        return $this->belongsTo(User::class, 'pelanggan_id' , 'id');
    }
    public function bengkel(){
        return $this->belongsTo(User::class, 'bengkel_id' , 'id');
    }
    public function service(){
        return $this->belongsTo(Service::class);
    }
    public function invoice(){
        return $this->hasOne(Invoice::class);
    }
}
