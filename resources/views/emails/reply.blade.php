@component('mail::message')
<?= $details['message'] ?>

@component('mail::button', ['url' => route('home')])
Visit IDEAKITA.ID 
@endcomponent

Thanks,<br>
{{ config('app.name') }}id
@endcomponent
