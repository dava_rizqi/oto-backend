<!DOCTYPE html>
<html lang="en">
  <head>
    <title>OTOFIX - LOGIN</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/admin')}}/dist/css/adminx.css" media="screen" />
  </head>
  <body>
    @yield('content')

    <!-- If you prefer jQuery these are the required scripts -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script src="{{asset('vendor/admin')}}/dist/js/vendor.js"></script>
    <script src="{{asset('vendor/admin')}}/dist/js/adminx.js"></script>

    <!-- If you prefer vanilla JS these are the only required scripts -->
    <!-- script src="{{asset('vendor/admin')}}/dist/js/vendor.js"></script>
    <script src="{{asset('vendor/admin')}}/dist/js/adminx.vanilla.js"></script-->
  </body>
</html>