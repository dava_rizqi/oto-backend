@extends('admin.index')

@section('content')
    <!-- BreadCrumb -->
    
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb adminx-page-breadcrumb">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item active  aria-current="page">{{$title}}</li>
        </ol>
      </nav>

      <div class="pb-3">
        <h1 class="text-left">{{$title}}</h1>
        <hr>
        <div class="text-left d-flex justify-content-between">
          <div>

        </div>
      </div>    
       
     @if (\Session::has('success'))
         <ul class="list-group border-0 mb-2">
             <li class="list-group-item list-group-item-success"><b>{!! \Session::get('success') !!}</b></li>
         </ul>
     @elseif($errors->any())
         <ul class="list-group border-0 mb-2">
             @foreach ($errors->all() as $error)
                 <li class="list-group-item list-group-item-danger"><b>{{ $error }}</b></li>
             @endforeach
         </ul>
     @endif

      <div class="row">
        <div class="col">
          <div class="card mb-grid">
            <div class="container float-left">
              
            <div class="table-responsive-md my-5" style="overflow-x: scroll !important">
              <table id="table" class="ui celled table ml-3 pb-4" style="width:100%;">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Email</th>
                    <th scope="col">Nama Pengguna</th>
                    <th scope="col">Role</th>
                    <th scope="col">Alamat/Kota</th>
                    <th scope="col">Nomor telepon</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($user as $item)
                  <tr>
                    <td>{{$item->id}}</td>
                    <td><small>{{$item->email}}</small></td>
                    <td><small>{{$item->nama}}</small></td>
                    <td><div class="badge badge-success badge-sm">{{$item->role_id == 2 ? 'Pelanggan' : 'Bengkel'}}</div></td>
                    <td><small>{{$item->alamat}} , {{$item->kota}}</small></td>
                    <td><small>{{$item->nohp}}</small></td>
                    <td>
                        <button class="btn btn-sm my-1 btn-outline-danger" 
                        data-toggle="tooltip" data-placement="top" onclick="deleteItem({{$item->id}})" title="Delete"><i class="fas fa-ban"></i></button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>

@endsection

@section('modal')
@endsection

@section('script')
<script>
  
function deleteItem(id){
  
  swal({
    title: "Are you sure?",
    text: "Data pesanan akan dihapus",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      axios.post(`{{route('user.destroy')}}` , {
        id : id
      }).then(res => {
          swal(res.data, {icon: "success"});
          window.location.reload()
      }).catch(err => {
        console.log(err.response)
      })
    }
  })
}
</script>
@endsection