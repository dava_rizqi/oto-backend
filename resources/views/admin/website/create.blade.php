@extends('admin.index')

@section('content')
    <!-- BreadCrumb -->
    
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb adminx-page-breadcrumb">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item active  aria-current="page">{{$title}}</li>
        </ol>
      </nav>

      <div class="pb-3">
        <h1 class="text-left">{{$title}}</h1>
        <hr>
        <div class="text-left d-flex justify-content-between">
          <div>
            {{-- <a href="{{route('post.export')}}" target="_blank" class="btn btn-outline-success btn-sm"><i class="fas mb-1 fa-file-excel"></i> Export menjadi excel</a> --}}
            <a href="{{route('post.create')}}" class="btn btn-outline-light btn-sm"><i class="fas mb-1 fa-book"></i> Create Setting</a></div>
        </div>
      </div>    
       
     @if (\Session::has('success'))
         <ul class="list-group border-0 mb-2">
             <li class="list-group-item list-group-item-success"><b>{!! \Session::get('success') !!}</b></li>
         </ul>
     @elseif($errors->any())
         <ul class="list-group border-0 mb-2">
             @foreach ($errors->all() as $error)
                 <li class="list-group-item list-group-item-danger"><b>{{ $error }}</b></li>
             @endforeach
         </ul>
     @endif

      <div class="row justify-content-center">
        <div class="col-lg-6 col-sm-12">
          <div class="card  mb-grid" style="background: white !important; color : black !important;">
            <div class="container my-4">
                <div class="form-group">
                    <label for="key" class="label">Name</label>
                    <input type="text" placeholder="name" id="key" name="key" class="form-control">
                </div>
                <div class="form-group">
                    <label for="value" class="label">Value</label>
                    <textarea placeholder="Enter value" name="value" class="form-control" id="value" cols="30" rows="10"></textarea>
                </div>
            </div>
          </div>
        </div>
      </div>

@endsection

@section('script')
<script>
    
</script>
@endsection