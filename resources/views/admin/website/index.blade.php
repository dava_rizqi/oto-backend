@extends('admin.index')

@section('content')
    <!-- BreadCrumb -->
    
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb adminx-page-breadcrumb">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item active  aria-current="page">{{$title}}</li>
        </ol>
      </nav>

      <div class="pb-3">
        <h1 class="text-left">{{$title}}</h1>
        <hr>
        <div class="text-left d-flex justify-content-between">
          <div>
            {{-- <a href="{{route('website.export')}}" target="_blank" class="btn btn-outline-success btn-sm"><i class="fas mb-1 fa-file-excel"></i> Export menjadi excel</a> --}}
            <a href="#" data-toggle="modal" data-target="#modal-auth" class="btn btn-light btn-sm"><i class="fas mb-1 fa-book"></i> Account Settings</a></div>
            <a href="#" data-toggle="modal" data-target="#modal-create" class="btn btn-light btn-sm"><i class="fas mb-1 fa-book"></i> Create Settings</a></div>
        </div>
      </div>    
       
     @if (\Session::has('success'))
         <ul class="list-group border-0 mb-2">
             <li class="list-group-item list-group-item-success"><b>{!! \Session::get('success') !!}</b></li>
         </ul>
     @elseif($errors->any())
         <ul class="list-group border-0 mb-2">
             @foreach ($errors->all() as $error)
                 <li class="list-group-item list-group-item-danger"><b>{{ $error }}</b></li>
             @endforeach
         </ul>
     @endif

      <div class="row">
        <div class="col">
          <div class="card mb-grid">
            <div class="container float-left">
              
            <div class="table-responsive-md">
              <table id="table" class="ui celled table ml-3 pb-4" style="width:100%;">
                <thead>
                  <tr>
                    <th scope="col">Group</th>
                    <th scope="col">Key</th>
                    <th scope="col" style="width : 100px !important">Value</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($website as $item)
                  <tr>
                    <td>{{$item->group}}</td>
                    <td>{{$item->key}}</td>
                    <td>{{substr($item->value , 0 ,100)}}</td>
                    <td>
                      <a href="#" onclick="getDetail({{$item->id}})" data-toggle="modal" data-target="#modal-edit" class="btn btn-sm my-1 btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pen"></i></a>
                      <button class="btn btn-sm my-1 btn-outline-danger" data-toggle="tooltip" data-placement="top" onclick="deleteItem({{$item->id}})" title="Delete"><i class="fas fa-ban"></i></button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>
{{--       
      <div class="row">
        <div class="col-lg-6 col-sm-12">
          @if (\Session::has('success-category'))
            <ul class="list-group border-0 my-2">
                <li class="list-group-item list-group-item-success"><b>{!! \Session::get('success') !!}</b></li>
            </ul>
          @endif
          <div class="card mb-grid">
            <div class="container float-left">
              
            <div class="table-responsive-md">
              <table id="table2" class="ui celled table ml-3 pb-4" style="width:100%;">
                <thead>
                  <tr>
                    <th scope="col">Key</th>
                    <th scope="col">Value</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($postCategories as $item)
                  <tr>
                    <td>{{$item->key}}</td>
                    <td>{{$item->value}}</td>
                    <td>
                      <a href="#" onclick="getDetail({{$item->id}})" data-toggle="modal" data-target="#modal-edit" class="btn btn-sm my-1 btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pen"></i></a>
                      <button class="btn btn-sm my-1 btn-outline-danger" data-toggle="tooltip" data-placement="top" onclick="deleteItem({{$item->id}})" title="Delete"><i class="fas fa-ban"></i></button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div> --}}

@endsection

@section('modal')

<form action="{{route('website.auth-update')}}" method="POST" enctype="multipart/form-data">
  @csrf
  <div class="modal fade" id="modal-auth" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" >
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title text-dark" id="exampleModalLabel">Account Setting</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body text-dark">
                  <div class="container">
                      <div class="row">
                          <div class="col">
                              <div class="container my-4">
                                  <div class="form-group">
                                      <label for="username" class="label">Username</label>
                                      <input type="text" placeholder="username" id="username" value="{{$user->email}}" name="username" class="form-control">
                                  </div>
                                  <div class="form-group">
                                      <label for="password" class="label">Password</label>
                                    <input type="password" placeholder="Your new password" id="password" name="password" class="form-control">
                                  </div>
                              </div>
                              
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                  </div>
              </div>
          </div>
      </div>
  </div>
</form>

<form action="{{route('website.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-dark" id="exampleModalLabel">New Setting</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-dark">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="container my-4">
                                    <div class="form-group">
                                        <label for="key" class="label">Key <small class="text-danger">*required</small></label>
                                        <input type="text" placeholder="name" id="key" name="key" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="group" class="label">Group <small class="text-danger">*required</small></label>
                                        <input type="text" placeholder="name" id="group" name="group" class="form-control">
                                    </div>
                                    <h4>Value Type</h4>
                                    <div class="form-group">
                                      <div onclick="textInput()">
                                          <input type="radio" id="text" checked name="type"> <label for="text">Text</label> <br>
                                      </div>
                                        <div onclick="fileInput()">
                                            <input type="radio" id="file" name="type"> <label for="file">File</label> <br>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group" id="text-input">
                                        <label for="value-text" class="label">Value</label>
                                        <textarea placeholder="Enter value" name="value_text" class="form-control" id="value-text" cols="30" rows="10"></textarea>
                                    </div>
                                    <div class="form-group" id="file-input">
                                        <label for="value-file" class="label">Value</label>
                                        <input type="file" name="value_file" class="form-control-file" id="value-file">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
  

<form action="{{route('website.update')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-dark" id="exampleModalLabel">Update Setting</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-dark">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="container my-4">
                                    <input type="hidden" placeholder="name" id="id_edit" name="id" class="form-control">

                                    <div class="form-group">
                                        <label for="key" class="label">Key <small class="text-danger">*required</small></label>
                                        <input type="text" placeholder="name" id="key_edit" name="key" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="group" class="label">Group <small class="text-danger">*required</small></label>
                                        <input type="text" placeholder="name" id="group_edit" name="group" class="form-control">
                                    </div>
                                    <h4>Value Type</h4>
                                    <div class="form-group">
                                        <div onclick="textInputEdit()">
                                            <input type="radio" id="text_edit" name="type"> <label for="text_edit">Text</label> <br>
                                        </div>
                                        <div onclick="fileInputEdit()">
                                            <input type="radio" id="file_edit" name="type"> <label for="file_edit">File</label> <br>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group" id="text-input-edit">
                                        <label for="value-text" class="label">Value</label>
                                        <textarea placeholder="Enter value" name="value_text" class="form-control" id="value-text-edit" cols="30" rows="10"></textarea>
                                    </div>
                                    <div class="form-group text-center" id="file-input-edit">
                                        <img src="" id="img-preview" height="200px" alt=""> <br>
                                        <label for="value-file text-left" class="label">Value</label>
                                        <input type="file" name="value_file" class="form-control-file" id="value-file-edit">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-outline-success">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@section('script')
<script>
    $(document).ready(function() {
        textInput();
        $('#table2').DataTable();
    })
    function textInput(){
        $('#file-input').hide();
        $('#text-input').show();
    }
    function fileInput(){
        $('#file-input').show();
        $('#text-input').hide();
    }
    function textInputEdit(){
        $('#file-input-edit').hide();
        $('#text-input-edit').show();
    }
    function fileInputEdit(){
        $('#file-input-edit').show();
        $('#text-input-edit').hide();
    }

function getDetail(id){
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    url : `{{route('website.detail')}}`,
    type : 'post',
    dataType : 'json',
    data : {id : id},
    success : function(data){
        $('#id_edit').val(data.id)
        $('#key_edit').val(data.key)
        $('#group_edit').val(data.group)
        var key_value = data.value
        if(key_value.includes(".jpg") || key_value.includes(".jpeg") || key_value.includes(".png")){
            // console.log('gambar')
            $('#img-preview').attr('src' , "{{asset('upload/setting')}}/" + JSON.parse(key_value))
            fileInputEdit()
            $('#text_edit').attr('checked' , false)
            $('#file_edit').attr('checked' , true)
        }else{
            $('#value-text-edit').val(JSON.parse(key_value))
            textInputEdit()
            $('#img-preview').attr('src' , null)
            $('#file_edit').attr('checked' , false)
            $('#text_edit').attr('checked' , true)
            // console.log('text')
        }
        $('#value-file-edit').val(data.id)
    },
  })
}
function convertMonth(month){
        if(month == 0){
          return 'January'
        }else if(month == 1){
          return 'February'
        }else if(month == 2){
          return 'March'
        }else if(month == 3){
          return 'April'
        }else if(month == 4){
          return 'May'
        }else if(month == 5){
          return 'June'
        }else if(month == 6){
          return 'July'
        }else if(month == 7){
          return 'August'
        }else if(month == 8){
          return 'September'
        }else if(month == 9){
          return 'October'
        }else if(month == 10){
          return 'november'
        }else if(month == 11){
          return 'December'
        }else if(month == 12){
          return 'February'
        }
      }
function setTransaction(kode){
  console.log(kode)
  $('#kode_produk_transaction').val(kode)
}
function deleteItem(id){
  
  swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to recover this imaginary file!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url : `{{route('website.destroy')}}`,
        type : 'post',
        dataType : 'json',
        data : {id : id},
        success : function(data){
          swal(data.message, {icon: "success"});
          window.location.reload()
        }
      })
    }
  })
}

</script>
@endsection