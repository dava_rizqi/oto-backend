@extends('admin.index')

@section('content')
    <!-- BreadCrumb -->
    
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb adminx-page-breadcrumb">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item active  aria-current="page">{{$title}}</li>
        </ol>
      </nav>

      <div class="pb-3">
        <h1 class="text-left">{{$title}}</h1>
        <hr>
        <div class="text-left d-flex justify-content-between">
          <div>
            {{-- <a href="{{route('post.export')}}" target="_blank" class="btn btn-outline-success btn-sm"><i class="fas mb-1 fa-file-excel"></i> Export menjadi excel</a> --}}
            <a href="#" data-toggle="modal" data-target="#modal-create" class="btn btn-light btn-sm"><i class="fas mb-1 fa-book"></i> Create Service</a></div>
        </div>
      </div>    
       
     @if (\Session::has('success'))
         <ul class="list-group border-0 mb-2">
             <li class="list-group-item list-group-item-success"><b>{!! \Session::get('success') !!}</b></li>
         </ul>
     @elseif($errors->any())
         <ul class="list-group border-0 mb-2">
             @foreach ($errors->all() as $error)
                 <li class="list-group-item list-group-item-danger"><b>{{ $error }}</b></li>
             @endforeach
         </ul>
     @endif

      <div class="row">
        <div class="col">
          <div class="card mb-grid">
            <div class="container float-left">
              
            <div class="table-responsive-md" style="overflow-x: scroll !important">
              <table id="table" class="ui celled table ml-3 pb-4" style="width:100%;">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Content</th>
                    <th scope="col">photo</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($service as $item)
                  <tr>
                    <td>{{$item->id}}</td>
                    <td><small>{{$item->name}}</small></td>
                    <td><small><?= $item->content?></small></td>
                    <td><img style="width : 50px" src="{{asset('upload/service/' .$item->photo)}}" class="img-fluid img-responsive" alt=""></td>
                    <td>
                        <a href="#" onclick="getDetail({{$item->id}})" data-toggle="modal" data-target="#modal-edit" class="btn btn-sm my-1 btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pen"></i></a>
                      <button class="btn btn-sm my-1 btn-outline-danger" data-toggle="tooltip" data-placement="top" onclick="deleteItem({{$item->id}})" title="Delete"><i class="fas fa-ban"></i></button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>

@endsection

@section('modal')
<form action="{{route('service.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal fade" id="modal-create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable modal-dialog-centered" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-dark" id="exampleModalLabel">New Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="container my-4">
                                    <div class="form-group">
                                        <label for="key" class="label">Service Name <small class="text-danger">*required</small></label>
                                        <input required type="text" placeholder="name" id="key" name="name" class="form-control">
                                    </div>
                                    <div class="form-group" id="text-input">
                                        <label for="value-text" class="label">Content</label>
                                        <textarea required placeholder="Enter value" name="content" class="form-control" id="content" cols="30" rows="10"></textarea>
                                    </div>
                                    <div class="form-group" id="file-input">
                                        <label for="value-file" class="label">Photo</label>
                                        <input required type="file" name="photo" class="form-control-file" id="value-file">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-outline-primary">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
  

<form action="{{route('service.update')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-dark" id="exampleModalLabel">Update Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="container my-4">
                                    <input type="hidden" name="id" id="id" >
                                    <div class="form-group">
                                        <label for="key" class="label">Service Name <small class="text-danger">*required</small></label>
                                        <input required type="text" placeholder="name" id="content-name" name="name" class="form-control">
                                    </div>
                                    <div class="form-group" id="text-input">
                                        <label for="value-text" class="label">Content</label>
                                        <textarea required placeholder="Enter value" name="content" class="form-control" id="content-content" cols="30" rows="10"></textarea>
                                    </div>
                                    <div class="form-group" id="file-input">
                                        <img src="" id="img-edit" style="width : 100%; position : relative" alt="">
                                        <label for="value-file" class="label">Photo</label>
                                        <input  type="file" name="photo" class="form-control-file" id="value-file">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-outline-success">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection
@section('script')
<script>
function getDetail(id){
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    url : `{{route('service.detail')}}`,
    type : 'post',
    dataType : 'json',
    data : {id : id},
    success : function(data){
      $('#content-name').val(data.name)
      $('#id').val(data.id)
      $('#content-content').html(data.content)
      $('#content-content').val(data.content)
      $('#img-edit').attr('src' , "{{asset('upload/service')}}" + '/' + data.photo)
    },
  })
}
function deleteItem(id){
  
  swal({
    title: "Are you sure?",
    text: "Once deleted, you will not be able to recover this imaginary file!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url : `{{route('service.destroy')}}`,
        type : 'post',
        dataType : 'json',
        data : {id : id},
        success : function(data){
          swal(data.message, {icon: "success"});
          window.location.reload()
        }
      })
    }
  })
}
$(document).ready(function () {
    new FroalaEditor('textarea', {
            // Set custom buttons.
            events: {
            'image.beforeUpload': function (files) {
                const editor = this
                if (files.length) {
                    var reader = new FileReader()
                    reader.onload = function (e) {
                    var result = e.target.result
                    editor.image.insert(result, null, null, editor.image.get())
                    }
                    reader.readAsDataURL(files[0])
                }
                return false
                }
            },  
            height: 300,
            toolbarButtons: {
            // Key represents the more button from the toolbar.
            moreText: {
                // List of buttons used in the  group.
                buttons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', 'fontFamily', 'fontSize', 'textColor', 'backgroundColor', 'inlineClass', 'inlineStyle', 'clearFormatting'],

                // Alignment of the group in the toolbar.
                align: 'center',

                // By default, 3 buttons are shown in the main toolbar. The rest of them are available when using the more button.
                buttonsVisible: 3
            },


            moreParagraph: {
                buttons: ['alignLeft', 'alignCenter', 'formatOLSimple', 'alignRight', 'alignJustify', 'formatOL', 'formatUL', 'paragraphFormat', 'paragraphStyle', 'lineHeight', 'outdent', 'indent', 'quote'],
                align: 'left',
                buttonsVisible: 3
            },

            moreRich: {
                buttons: ['insertLink', 'insertImage', 'insertVideo', 'insertTable', 'emoticons', 'fontAwesome', 'specialCharacters', 'embedly', 'insertFile', 'insertHR'],
                align: 'left',
                buttonsVisible: 3
            },

            moreMisc: {
                buttons: ['undo', 'redo', 'fullscreen', 'print', 'getPDF', 'spellChecker', 'selectAll', 'html', 'help'],
                align: 'right',
                buttonsVisible: 2
            }
            },

            // Change buttons for XS screen.
            })
})
</script>
@endsection