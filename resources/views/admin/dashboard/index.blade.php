@extends('admin.index')

@section('content')
    <!-- BreadCrumb -->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb adminx-page-breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
        </ol>
    </nav>

    <div class="pb-3">
        <h1>Dashboard</h1>
    </div>

    <div class="row justify-content-center">
        <div class="col-12 text-center">
            <img src="https://i.ibb.co/v1J1vmJ/photo-removebg-preview.png" style="width : 390px !important" class="d-inline-block align-top mr-2 font-weight-bold" alt="">
        </div>
    </div>
@endsection

@section('script')
@endsection