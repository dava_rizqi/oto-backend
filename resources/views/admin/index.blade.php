<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin.template.head')
  </head>
  <body >
    <div class="adminx-container">
      @include('admin.template.header')

      <!-- expand-hover push -->
      <!-- Sidebar -->
      
      @include('admin.template.sidebar')

      <!-- adminx-content-aside -->
      <div class="adminx-content">
        <!-- <div class="adminx-aside">

        </div> -->
        <div class="adminx-main-content">
          <div class="container-fluid">
              @yield('content')
              @include('admin.template.footer')
          </div>
        </div>
      </div>
    </div>
    @include('admin.template.script')
    @include('sweet::alert')
    @yield('script')
    @yield('modal')
  </body>
</html>