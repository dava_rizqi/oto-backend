@extends('admin.index')

@section('content')
    <!-- BreadCrumb -->
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb adminx-page-breadcrumb">
          <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
          <li class="breadcrumb-item active  aria-current="page">{{$title}}</li>
        </ol>
      </nav>

      <div class="pb-3">
        <h1 class="text-left">{{$title}}</h1>
        <hr>
        <div class="text-left d-flex justify-content-between">
          <div>
            {{-- <a href="{{route('post.export')}}" target="_blank" class="btn btn-outline-success btn-sm"><i class="fas mb-1 fa-file-excel"></i> Export menjadi excel</a> --}}
            <a href="#" data-toggle="modal" data-target="#modal-create" class="btn btn-light btn-sm"><i class="fas mb-1 fa-book"></i> Create Service</a></div>
        </div>
      </div>    
       
     @if (\Session::has('success'))
         <ul class="list-group border-0 mb-2">
             <li class="list-group-item list-group-item-success"><b>{!! \Session::get('success') !!}</b></li>
         </ul>
     @elseif($errors->any())
         <ul class="list-group border-0 mb-2">
             @foreach ($errors->all() as $error)
                 <li class="list-group-item list-group-item-danger"><b>{{ $error }}</b></li>
             @endforeach
         </ul>
     @endif

      <div class="row">
        <div class="col">
          <div class="card mb-grid">
            <div class="container float-left">
              
            <div class="table-responsive-md my-5" style="overflow-x: scroll !important">
              <table id="table" class="ui celled table ml-3 pb-4" style="width:100%;">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Nama Layanan</th>
                    <th scope="col">Nama Pelanggan</th>
                    <th scope="col">Nama Bengkel</th>
                    <th scope="col">Waktu Mulai</th>
                    <th scope="col">Waktu Selesai</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($order as $item)
                  <tr>
                    <td>{{$item->id}}</td>
                    <td><small>{{$item->service->nama_layanan}}</small></td>
                    <td><small>{{$item->pelanggan->nama}}</small></td>
                    <td><small>{{$item->bengkel->nama}}</small></td>
                    <td><small>{{$item->waktu_dimulai}}</small></td>
                    <td><small>{{$item->waktu_selesai}}</small></td>
                    <td><div class="badge badge-success badge-sm">{{$item->status}}</div></td>
                    <td>
                        {{-- <a href="#" onclick="getDetail({{$item->id}})" data-toggle="modal" data-target="#modal-edit" class="btn btn-sm my-1 btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-pen"></i></a> --}}
                        <button class="btn btn-sm my-1 btn-outline-danger" 
                        data-toggle="tooltip" data-placement="top" onclick="deleteItem({{$item->id}})" title="Delete"><i class="fas fa-ban"></i></button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>

@endsection

@section('modal')
{{-- <form action="{{route('order.update')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-dark" id="exampleModalLabel">Update Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="container my-4">
                                    <input type="hidden" name="id" id="id" >
                                    <div class="form-group">
                                        <label for="Id" class="label">Order Id <small class="text-danger">*required</small></label>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-sm btn-outline-success">Update</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form> --}}
@endsection

@section('script')

<script>
  
function deleteItem(id){
  
  swal({
    title: "Are you sure?",
    text: "Data pesanan akan dihapus",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      axios.post(`{{route('order.destroy')}}` , {
        id : id
      }).then(res => {
          swal(res.data, {icon: "success"});
          window.location.reload()
      }).catch(err => {
        console.log(err.response)
      })
    }
  })
}
</script>
@endsection