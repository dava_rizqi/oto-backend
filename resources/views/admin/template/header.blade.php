
      <nav class="navbar navbar-expand navbar-light justify-content-between fixed-top">
        <a class="navbar-brand mb-0 h1 d-none d-md-block" href="{{url('/admin')}}">
          <img src="https://i.ibb.co/v1J1vmJ/photo-removebg-preview.png" style="width : 90px !important" class="d-inline-block align-top mr-2 font-weight-bold" alt="">
        </a>

        <div class="d-flex flex-1 d-block d-md-none">
          <a href="#" class="sidebar-toggle ml-3">
            <i data-feather="menu"></i>
          </a>
        </div>

        <ul class="navbar-nav d-flex justify-content-end mr-2">
          <!-- Notifications -->
          <li class="nav-item dropdown">
            <a class="nav-link avatar-with-name" id="navbarDropdownMenuLink" data-toggle="dropdown" href="#">
              <img src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/128.jpg" class="d-inline-block align-top" alt="">
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
              {{-- <a class="dropdown-item" href="{{route('website.index')}}">Settings</a> --}}
              <div class="dropdown-divider"></div>
              <form action="{{route('logout')}}" method="post">
                @csrf
                <button class="dropdown-item text-danger" type="submit" href="#">Sign out</button>
              </form>
            </div>
          </li>
        </ul>
      </nav>