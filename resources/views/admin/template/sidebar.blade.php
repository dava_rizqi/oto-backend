
<div class="adminx-sidebar expand-hover push">
<ul class="sidebar-nav">
        <li class="sidebar-nav-item my-3">
            <a href="{{route('dashboard')}}" class="sidebar-nav-link active">
                <span class="sidebar-nav-icon">
                <i data-feather="home"></i>
                </span>
                <span class="sidebar-nav-name">
                Dashboard
                </span>
                <span class="sidebar-nav-end">

                </span>
            </a>
        </li>
        <li class="sidebar-nav-item my-3">
            <a href="{{route('order.index')}}" class="sidebar-nav-link">
                <span class="sidebar-nav-icon">
                    <i class="fas fa-box"></i>
                </span>
                <span class="sidebar-nav-name">
                Pesanan
                </span>
            </a>
        </li>
        <li class="sidebar-nav-item my-3">
            <a href="{{route('user.index')}}" class="sidebar-nav-link">
                <span class="sidebar-nav-icon">
                    <i class="fas fa-box"></i>
                </span>
                <span class="sidebar-nav-name">
                Pengguna
                </span>
            </a>
        </li>

</div><!-- Sidebar End -->