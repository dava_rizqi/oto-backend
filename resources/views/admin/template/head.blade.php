
    <title>OTOFIX - Data Management</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendor/admin')}}/dist/css/adminx.css" media="screen" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.semanticui.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    
    <style>
      @media (min-width: 768px) {
        .modal-xl {
          width: 95%;
          max-width:1200px;
        } 
      }
      .card{
        border-radius: 10px !important;
        box-shadow: 5px 5px 15px rgba(0,0, 0,0.2);
        background-color: rgb(255, 255, 255) !important;
        border : none !important
      }
    </style>