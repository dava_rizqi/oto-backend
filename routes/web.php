<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index')->middleware('auth')->name('dashboard');
Auth::routes();

Route::prefix('order')->namespace('admin')->middleware('auth')->group(function () {
    Route::get('/', 'OrderController@index')->name('order.index');
    Route::get('/create', 'OrderController@create')->name('order.create');
    Route::post('/', 'OrderController@store')->name('order.store');
    Route::post('/destroy', 'OrderController@destroy')->name('order.destroy');
    Route::post('/detail', 'OrderController@detail')->name('order.detail');
    Route::get('/{id}/edit', 'OrderController@edit')->name('order.edit');
    Route::post('/update', 'OrderController@update')->name('order.update');
    Route::post('/auth-update', 'OrderController@authUpdate')->name('order.auth-update');
});
Route::prefix('user')->namespace('admin')->middleware('auth')->group(function () {
    Route::get('/', 'UserController@index')->name('user.index');
    Route::get('/create', 'UserController@create')->name('user.create');
    Route::post('/', 'UserController@store')->name('user.store');
    Route::post('/destroy', 'UserController@destroy')->name('user.destroy');
    Route::post('/detail', 'UserController@detail')->name('user.detail');
    Route::get('/{id}/edit', 'UserController@edit')->name('user.edit');
    Route::post('/update', 'UserController@update')->name('user.update');
    Route::post('/auth-update', 'UserController@authUpdate')->name('user.auth-update');
});