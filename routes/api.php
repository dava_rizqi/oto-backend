<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/test', 'OrderController@invoice');
// rute api :
// http://localhost:8000/api/login
Route::prefix('users')->group(function () {
    Route::post('/login', 'UserController@login');
    Route::post('/register', 'UserController@register');
    Route::post('/complete-profile', 'UserController@completeProfile');
    Route::post('/complete-role' , 'UserController@completeRole');
    Route::post('/edit-profile', 'UserController@editProfile');
    
    Route::post('/search', 'UserController@searchUser');
    Route::post('/all', 'UserController@getAllUser');
    Route::post('/all/paginate', 'UserController@getPaginateUser');
    Route::post('/detail', 'UserController@getDetailUser');
});

Route::prefix('orders')->group(function () {
    Route::post('/all', 'OrderController@getAllOrder');
    Route::post('/all/paginate', 'OrderController@getPaginateOrder');
    Route::post('/detail', 'OrderController@getDetailOrder');

    Route::post('/filter/status', 'OrderController@filterStatus');
    Route::post('/filter/keyword', 'OrderController@filterKeyword');

    Route::post('/update/status', 'OrderController@updateStatus');
    Route::post('/store', 'OrderController@store');
});


Route::prefix('services')->group(function () {
    Route::post('/all', 'ServiceController@getAllService');
    Route::post('/category/get', 'ServiceController@getAllCategories');
    Route::post('/all/paginate', 'ServiceController@getPaginateService');
    Route::post('/detail', 'ServiceController@getDetailService');
    Route::post('/store', 'ServiceController@store');
    Route::post('/category/store', 'ServiceController@storeCategory');
    Route::post('/category/delete', 'ServiceController@deleteCategory');
    Route::post('/update', 'ServiceController@update');
    Route::post('/delete', 'ServiceController@destroy');
});